import React,{Component} from 'react';
import {Text,View,StyleSheet,TextInput,TouchableHighlight,Image,ImageBackground,FlatList} from 'react-native';
import {createAppContainer, withNavigationFocus,StackActions, SafeAreaView  } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Login from './Login';
import Home from './Home';
import Splash from './Splash';
import Scanner from './Scanner'

const Route = createStackNavigator({
    Splash:{ screen: Splash},
    Login: { screen: Login }, 
    Home: { screen: Home }, 
    Scanner : { screen: Scanner }
    

  },
  
  {
    headerMode:false,
    initialRouteName: 'Home',
    unmountOnBlur: true
  }
);

const App = createAppContainer(Route)  
export default App;
