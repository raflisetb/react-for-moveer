import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import AsyncStorage from '@react-native-community/async-storage';
import { View,BackHandler,Alert,Platform,StatusBar } from 'react-native';



export default class Home extends React.Component {
  constructor(props){  
    super(props);
    this.state = {
      loginUrl:'', 
    }
    
  }

  webView = {
    canGoBack: false,
    ref: null,
    backClickCount: 0,
    firstpage : true 
  }

  onAndroidBackPress = () => {
    
    if (this.webView.ref.startUrl == 'https://moveer.org/user/companies' ) { 
      console.log('hello exit')
        return this.exitCheck() 
    }
  
    if( this.webView.ref.startUrl != 'https://moveer.org/user' ) {
      this.webView.ref.goBack();
      return true;
    }
  
    else { 
      
      return this.exitCheck() 
    }
    return false;
  }

  
  exitCheck = () => { 
    if (this.webView.backClickCount == 0 ) { 
      Alert.alert(
        'Exit Apps',
        'Click back button once again to exit the apps',
        [

          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
        this.webView.backClickCount = 1
          return true
    }
    else { 
      BackHandler.exitApp();
    }
  }


  componentWillMount() {
   
      BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    
  }

  componentWillUnmount() {

      BackHandler.removeEventListener("hardwareBackPress");
    
  }


  componentDidMount= async() => { 
   
    this.initiate() ;

    
   }

   

   componentDidUpdate = async() => { 
    this.webview()
   }


   

   initiate= async() => { 
     
   let value =await AsyncStorage.getItem('@login-url');
    console.log('valueData ',value);
      if (value !== null) {
        let res = JSON.parse(value)
        
        if(res.status_code == 200){
     
           
            this.setState({
              loginUrl:'https://moveer.org'+res.login_url,
              alreadyLogin :true
            })
           
        
            const isFocused = this.props.navigation.isFocused();
            console.log('focused home', isFocused)
         
        }
        else {
                console.log('error login')
              
                this.props.navigation.navigate('Login', {
                onGoBack: () => this.initiate(),
              });
         
        }
      }
      else {
          console.log('error value')
        
          this.props.navigation.navigate('Login', {
            onGoBack: () => this.initiate(),
          });
         
      }
      

   } 
      



  changeUrl = async () => { 
    let url = await AsyncStorage.getItem('@redirect-data')
    
    this.setState({
      loginUrl: url
    })
  } 

  _onNavigationStateChange = (web_view_state) => { 
    console.log(web_view_state.url)
    if(web_view_state.url == 'https://moveer.org/en/user/companies/scan')
    { 
      this.initiate()

      this.props.navigation.navigate('Scanner', {
        onGoBack: () => this.changeUrl(),
      });
    }
    if(web_view_state.url == "https://moveer.org/en/logout") {
      this.clearLogout()
      
    }
    
  }

  clearLogout = async() => {
    try { 
        let delete_data = await AsyncStorage.clear();
    } catch(e) { 
      console.log("error" ,e)
    }
    this.props.navigation.navigate('Login')
  }


  webview = () => { 

    return ( 
       
    <WebView
    ref={(webView) => { this.webView.ref = webView; }}
    source={{ uri:this.state.loginUrl }}
    geolocationEnabled={true}
    style={{marginTop: (Platform.OS === 'ios') ? 36 : 0}}
    onNavigationStateChange={this._onNavigationStateChange.bind(this)}
    
    />
    
    )
  }

   

  render() {
    
    return ( 
      this.webview()
    );
  }
}