import React, { Component } from 'react';
import {Text,View,TextInput, ScrollView, TouchableHighlight,TouchableOpacity,Image,ImageBackground,Picker, Button, Alert, Modal, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { RNCamera } from 'react-native-camera'
import styles from './style/Style-scanner'
import { withNavigationFocus } from 'react-navigation'


export default class Scanner extends React.Component {

    constructor(props){
        super(props);
        this.state = {
			focusedScreen: true,
            isLoading:true, 
            torchOn:false
		  }
    }

    componentDidMount() {
      const isActive = this.props.navigation.isFocused()
      if(isActive == true){
        this.renderCamera()
          let that = this;
          setTimeout(function () {
              that.setState({
                  isLoading:false
              });
          },2500);
      }
      else {
        return null
      }
    }


    renderCamera = () => {


        if(this.state.isLoading == true) {
            console.log('going to load react native camera');
        } else {
            console.log('active');
            return(
                <RNCamera
                    style={styles.cameraScanner}
                    flashMode= {this.state.torchOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
                    type={RNCamera.Constants.Type.back}
                    onGoogleVisionBarcodesDetected={this.barcodeRecognized} 
                    captureAudio={false}
                >
                </RNCamera>
            )
        }
      }



    // componentDidMount() {
    //
    //     navigation.addListener('willFocus', () =>
    //       this.setState({ focusedScreen: true })
    //     );
    //     navigation.addListener('willBlur', () =>
    //       this.setState({ focusedScreen: false })
    //     );
    //   }



    goBack= ()=> {
        this.setUrl('https://moveer.org/user/companies')
    }

    barcodeRecognized = ({ barcodes }) => {
        barcodes.forEach(barcode => this.setUrl(barcode.data))

      };


    setUrl= async   (url) => {
        console.log('scanner' , url)
       
        await AsyncStorage.setItem('@redirect-data', url )
        let uri = await AsyncStorage.getItem('@redirect-data')
        console.log(uri)

        this.props.navigation.state.params.onGoBack();
         this.props.navigation.goBack();
    }

    inputForm= (text) => {
        let url = 'https://moveer.org/api/agent-code/3VUfhUZuM12uRvh/'
        fetch(url+text)
        .then((response) => response.json())
            .then((json) => {
               if(json.status == true) { 
                  this.setUrl(json.data.link)
               }
               else { 
                   console.log('error')
               }
            })
            
    }

    setTorch= () => {
        if (this.state.torchOn == false) { 
            this.setState({
                torchOn : true
            })
        }
        else { 
            this.setState({
                torchOn : false
            })
        }
    }

   render() {

       return (
        <ImageBackground
                source={require('../images/mask-scan-bg.jpg')}
                style = {styles.backgroundImage}>

            <View style = {styles.headIcon}>
                <TouchableOpacity activeOpacity={1} onPress={()=>this.goBack()} >
                <Image source={require('../images/cancel.png')} style={styles.cancelButton}/>
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={1} onPress={()=>this.setTorch()} >
                <Image source={require('../images/torch.png')} style={styles.flashButton}/>
                </TouchableOpacity>
            </View>

            <View style= {styles.scannerDiv} >
            <ImageBackground
                source={require('../images/scanner.png')}
                style = {styles.scanner}>
                <View style= {styles.scannerColumn} >
                {this.renderCamera()}
                </View>
            </ImageBackground>
            <View style={{padding: 10}}>
                <TextInput
                    style={styles.code}
                    placeholder="Insert Code"
                    onChangeText= {text => this.inputForm(text)}
                />
            </View>

            <View style={{marginTop:30}}>

            <Text style={{color:"white",  fontSize:25, alignSelf:"center"}}>
                Fill code or Scan 
                </Text>


                <Text style={{color:"white",  fontSize:25, alignSelf:"center"}}>
                to get mask or food
                </Text>


            </View>
        </View>


        </ImageBackground>
       )


   }





}

