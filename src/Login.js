import React,{Component} from 'react';
import {Text,View,TextInput, ScrollView, TouchableHighlight,TouchableOpacity,Image,ImageBackground,Picker, Button, Alert, Modal, Platform} from 'react-native';
import CheckBox from 'react-native-check-box';
import RNPickerSelect from 'react-native-picker-select';
import { LoginButton, AccessToken, LoginManager, ShareDialog, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';
import { Dropdown } from 'react-native-material-dropdown';
import appleAuth , {  AppleAuthRequestOperation, AppleAuthRequestScope,AppleAuthCredentialState } from '@invertase/react-native-apple-authentication';
import {request, PERMISSIONS , requestMultiple} from 'react-native-permissions';
import styles from './style/Style-login'

export default class Login extends React.Component{
    constructor(props){

        super(props);
        this.state={
            isChecked:false,
            data : [],
            selectedValue:'',
            country: [], 
            id_country : '',
            disabled:true,
            loginUrl:'', 
            modalVisible:false, 
            selecLanguage: 'Select Language', 


        } 
    }
    
    componentDidMount= async() => { 


      let check_auth = appleAuth.isSupported
      console.log("apple auth", check_auth)

      
       let value =await AsyncStorage.getItem('@user-data');
       console.log('value ',value);
        if (value !== null) {
            const userdata = JSON.parse(value);
            this.getHomeUrl(userdata); 

           // console.log('userdata ',userdata);
            if(userdata.provider_id){
            //  this.props.navigation.navigate('Home');
            }
          }
        GoogleSignin.configure({
          scopes: ['profile', 'email'],
          // androidClientId:'649864484048-stg5tfj01ji4k5k4hbnh91ikak7pjq1n.apps.googleusercontent.com',
          webClientId:"63822363665-teofk1telpv28alrjbnllmfih6l5tps6.apps.googleusercontent.com",
          offlineAccess: false,
          forceConsentPrompt: true
        });
      
        this.getLanguange()
        this.getCountry()
        this.requestPermission()
    } 
    
    getLanguange = async() => { 
      fetch('https://moveer.org/api/getlang')
            .then((response) => response.json())
            .then((json) => {
              
              //this.setState({data:json.data})
              var count = json.data.length
              const drop_down_data = json.data.map(element => 
                ({
                value: element.code,
                label:"     "+element.name
               })
              )

              this.setState({data:drop_down_data})
            })
            .catch((error) => {
              console.error(error);
            });
    }


    getCountry() { 
      fetch('https://moveer.org/api/country/mobile')
      .then((response) => response.json())
      .then((json) => {
        
        //this.setState({data:json.data})
        var count = json.data.length
        // const drop_down_data = json.data.map(element => 
        //   ({
        //   value: element.code,
        //   label: element.name
        //  })
        // )

        this.setState({country:json.data})
      
      })
      .catch((error) => {
        console.error(error);
      });

    }

    requestPermission() { 

      if (Platform.OS === 'android') { 
        
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
        PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION;
        PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION;

       
          request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {      
          });
       
      }
        
     }

   

    showCountry() { 
      console.log('kirim',this.state.country)
     
      return this.state.country.map((item)=>{
         return <Text>hahaha</Text>
      })
      
    }

    selectCountry(value) { 
      this.setState({
        id_country : value,
        modalVisible:false
      })
       
    }

    getPublicProfile = async () => {
        const infoRequest = new GraphRequest(
          '/me?fields=id,name,picture,email',
          null,
          (error, result) => {
            if (error) {
              console.log('Error fetching data: ' + error.toString());
            } else {
              let data = {
                    "provider":"facebook",
                    "provider_id":result.id,
                    "name":result.name?result.name:'',
                    "email":result.email?result.email:'Not available',
                    "token":"XF6fQC7LH5Z0ckqr9Ipx"
                }
                
                this.getHomeUrl(data);
                
            }
          }
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      }


    alertTof = () =>{
      Alert.alert(
        "Terms Coditions & Language",
        "Term Condition & Language must be filled",
        [
          { text: "OK", onPress: () => console.log("OK tos Pressed") }
        ],
        { cancelable: false }
      ); 
        
    }

    alertEmail = () => { 
       Alert.alert(
        "Please Fill your email ",
        "please provide your email",
        [
          { text: "OK", onPress: () => console.log("OK tos Pressed") }
        ],
        { cancelable: false }
      ); 
    }

    facebookLogin = ()=>{

      const that = this;
      LoginManager.logInWithPermissions(["public_profile"]).then(
          function(result) {
            if (result.isCancelled) {
              console.log("Login cancelled");
            } else {
              AccessToken.getCurrentAccessToken().then(
              (data) => {
                  console.log(data.accessToken.toString())}
              )
              that.getPublicProfile();
            }
          },
          function(error) {
            console.log("Login fail with error: " + error);
          }
        )
    }

    googleLogin = async()=>{
        try {
            await GoogleSignin.hasPlayServices({
                showPlayServicesUpdateDialog: true,
              });
              const userInfo = await GoogleSignin.signIn();
              console.log(userInfo);
              if(userInfo){
                let data = {
                    "provider":"google",
                    "provider_id":userInfo.user.id,
                    "name":userInfo.user.name?userInfo.user.name:'Not available',
                    "email":userInfo.user.email?userInfo.user.email:'Not available',
                    "token":"XF6fQC7LH5Z0ckqr9Ipx"
                }
                this.getHomeUrl(data);
              }
            } catch (error) {
              if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('User Cancelled the Login Flow');
              } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('Signing In');
              } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log('Play Services Not Available or Outdated');
              } else {
                console.log('Some Other Error Happened', error);
              }
        }
     
    }

    _revokeAccess = async () => {
        //Remove your application from the user authorized applications.
        try {
            await GoogleSignin.revokeAccess();
        } catch (error) {
            console.error(error);
        }
    }
    
    getHomeUrl = (data) =>{
      console.log('disini')
        AsyncStorage.setItem('@user-data', JSON.stringify(data))
        let obj ={
          method: 'POST',
          headers: {     
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
            // post to laravel need to be json stringify 
          body: JSON.stringify(data)
        }
        
        fetch('https://moveer.org/api/login/mobile', obj)
        .then((response)=>response.json())
        .then((json)=>{
         console.log(json)
          if(json.status_code == 200){
            AsyncStorage.setItem('@login-url',JSON.stringify(json));
            
            // goback 
            this.props.navigation.state.params.onGoBack();
            this.props.navigation.goBack();
            
          }
        }).catch((error)=> {
           console.log('There has been a problem with your fetch operation: ' + error.message);
        });
    }
    
    appleLoginButton () { 
      if (Platform.OS === 'ios') {
        return (
            <TouchableOpacity  activeOpacity={1} onPress={()=>this.onAppleButtonPress()}>
                      <View  style={styles.socialIcon}>
                          <Image source={require('../images/apple-black.png')}/>
                          <Text style={styles.socialText}>Continue with Apple ID</Text>
                      </View>
            </TouchableOpacity>
        )
      }
    }

    appleLoginButtonTOF () { 
        if (Platform.OS === 'ios') {
          return (
             <TouchableOpacity  activeOpacity={1} onPress={()=>this.alertTof()}>
                        <View  style={styles.socialIcon}>
                            <Image source={require('../images/apple-black.png')}/>
                            <Text style={styles.socialText}>Continue with Apple ID</Text>
                        </View>
              </TouchableOpacity>
          )
        }

      }
    onAppleButtonPress= async () => {
      
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
      });
      // WILL USE IT LATER !!!!!
      // get current authentication state for user
      // const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
      // use credentialState response to ensure the user is authenticated
      // if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
      //   console.log('logged')
      // }
      // else { 
      //   console.log('error') 000616.30228a25cad14c1fbe019525b0941b38.0936
      // }

      if (appleAuthRequestResponse.email == null || appleAuthRequestResponse.email == '') { 
        this.alertEmail() 
      }
      
      else { 
        let data = {
                    "provider":"apple",
                    "provider_id":appleAuthRequestResponse.user,
                    "name":appleAuthRequestResponse.fullName.givenName + " " + appleAuthRequestResponse.fullName.familyName,
                    "email":appleAuthRequestResponse.email?appleAuthRequestResponse.email:'Not available',
                    "token":"XF6fQC7LH5Z0ckqr9Ipx"
                }
      
      this.getHomeUrl(data);
      }
      
    }

    
  
  
    render(){
      
        return(
            <ImageBackground
                source={require('../images/Login-bg.png')} 
                style = {styles.backgroundImage}>
                <ScrollView style={styles.mainContainer}>
                <TouchableOpacity activeOpacity={1} onPress={()=> {this.setState({modalVisible:true}) } } >
                  <Image style={styles.flag} source={require("../images/flag-button-round-250.png")} />
                </TouchableOpacity>
            
                    {/* <Image style={styles.logo} source={require("../images/Vector-Smart-Object.png")}/> */}
                    <Image style={styles.wide_logo} source={require("../images/wide_logo.png")}/>
                    <Text style={styles.text}> Continue to Login </Text> 
                   <Dropdown
                        baseColor="gray"
                        selectedItemStyle={{fontFamily: 'Montserrat-Light',paddingLeft:20}}
                        labelTextStyle = {{fontFamily: 'Montserrat-Light',paddingLeft:20}}
                        inputContainerStyle={{ borderBottomColor: 'transparent',marginTop:-10 }} 
                        value={this.state.label}
                        containerStyle={styles.dropDownContainer}
                        label={this.state.selecLanguage}
                        
                        itemTextStyle ={{fontFamily: 'Montserrat-Light', marginLeft:20}}
                        data={this.state.data}
                        onChangeText={(value, label)=> {this.setState({
                          selectedValue:value, 
                          selecLanguage:null
                        });}}
        
                    />
                    {
                       console.log(this.state.isChecked,'checked',this.state.selectedValue,'selected'),
                       (this.state.isChecked && this.state.selectedValue != '') ?
                      
                    <View>
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.facebookLogin()} >
                        <View style={styles.socialIcon}>
                            <Image source={require('../images/facebook.png')}/>
                            <Text style={styles.socialText}>Continue with Facebook</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  activeOpacity={1} onPress={()=>this.googleLogin()} >
                        <View  style={styles.socialIcon}>
                            <Image source={require('../images/google.png')}/>
                            <Text style={styles.socialText}>Continue with Google</Text>
                        </View>
                    </TouchableOpacity>
                     {this.appleLoginButton()}

                    </View>
                    
                    :
                    <View>
                      <TouchableOpacity activeOpacity={1} onPress={()=>this.alertTof()} >
                        <View style={styles.socialIcon}>
                            <Image source={require('../images/facebook.png')}/>
                            <Text style={styles.socialText}>Continue with Facebook</Text>
                        </View>
                      </TouchableOpacity>
                      <TouchableOpacity  activeOpacity={1} onPress={()=>this.alertTof()}>
                        <View  style={styles.socialIcon}>
                            <Image source={require('../images/google.png')}/>
                            <Text style={styles.socialText}>Continue with Google</Text>
                        </View>
                    </TouchableOpacity>
                       {this.appleLoginButtonTOF()}
                   
                   
                  </View>
                  }
                    <CheckBox style={styles.checkbox} onClick={()=>{this.setState({isChecked:!this.state.isChecked })}} isChecked={this.state.isChecked}
                        rightText={"I agree Terms & Conditions"}   rightTextStyle={{fontFamily: 'Montserrat-Light',fontSize:12,color:'gray'}} checkBoxColor="#ced4da"
                        />

                  {/* Modal Country  */}
                  <Modal
                    animationType="slide"
                    transparent={true}
                     position={'bottom'}
                    visible= {this.state.modalVisible}
                    onRequestClose={() => {
                      
                     
                    }}
                  >
                    
                      <View style={styles.modalView}>
                        
                        
                        {
                          this.state.country.map((item)=>{
                            let image_url = "https://moveer.org/storage/"+ item.image
                            return  <TouchableOpacity  style={styles.row1} onPress={()=>this.selectCountry(item.id)} >
                                 <View  style={[styles.box_image]}>
                                   <Image style= {[styles.img_country]}  source={{uri:image_url}} />
                                   
                                 </View>
                              <View  style={[ styles.two]}> 
                               <Text style= {{fontFamily: 'Montserrat-Light',fontSize:20, textAlign:"left"}} > {item.name}</Text>
                              </View>
                            </TouchableOpacity> 
                         })
                        }
                         
                         <View style={{ flex: 1, marginTop:250, flexDirection: 'row',height:100,justifyContent:"flex-end"}}>  
                          <View style={{alignItems:"flex-end"}}> 
                            <Text style={{fontFamily: 'Montserrat-Light',fontSize:20, textAlign:"left",  fontWeight:"bold"}} >Select Country</Text> 
                          </View>

                          <View style={{alignItems:"flex-end",marginLeft:'50%' }}> 
                            <TouchableHighlight
                                onPress={()=> {this.setState({modalVisible:false}) } }
                            >
                                <Image style={{width:15, height:20, marginTop:7}} source={require("../images/x.png")} />
                          </TouchableHighlight>

                          </View>
                        </View>
                        
                      </View>
                   
                  </Modal>

                </ScrollView>
            </ImageBackground>

        );
    }
}
