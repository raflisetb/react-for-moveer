import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    backgroundImage : {
        resizeMode: 'cover',
        flex:1,
        width:null,
        height:null
    },

    headIcon : { 
        flex:1, 
        marginTop: 30,
        flexDirection:'row',
       
    },
    code : { 
        height:40, 
        width: "70%",
        backgroundColor:"white",
        alignSelf : "center",
        borderRadius: 10,

    },
    cancelButton : { 
       marginLeft:30,
       alignSelf : "flex-start"
    },

    flashButton : { 
       
        marginLeft:315,
        alignSelf : "flex-end"
 
     },

    scannerDiv : { 
        flex: 2 
       
    }, 

    scanner : { 
        alignSelf : 'center',
        flexDirection : 'row',
        width:240,
        height:240

    },
    cameraScanner : { 
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        borderRadius:20,
        overflow: 'hidden',
    },

    
    scannerColumn : { 
        alignSelf : 'center',
        flexDirection : 'row',
        width:220,
        height:220,
        borderRadius:10,
       marginLeft:10

    },

    



}) 


export default styles;