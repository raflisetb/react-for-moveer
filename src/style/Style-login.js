import {StyleSheet} from 'react-native'

 const styles = StyleSheet.create({

        backgroundImage : {
          resizeMode: 'cover',
          flex:1,
          width:null,
          height:null
      },
      mainContainer:{
      
          flex:1,
      },
      flag:{
          marginTop:40,
          marginRight:30,
          alignSelf:'flex-end',
      },
      logo:{
      
          resizeMode:'contain',
          width:500,
          height:250,
          alignSelf:'center',
      },
      covidText:{
      
          marginTop:10,
          textAlign:'center',
          fontSize:24,
          fontFamily: 'Montserrat-Bold',
          fontWeight:'600',
          color:'#f29018'
      },
      text:{
          textAlign:'center',
          marginTop:10,
          fontSize:13,
          fontFamily:'Montserrat-Light',
          color:'gray'
      },
      dropDownContainer:{
          marginTop:40,
          width:"90%",
          display:'flex',
          alignSelf:'center',
          borderWidth:1,
          borderColor:'#ced4da',
          borderRadius:5,
      },
      socialIcon:{
      
          display:"flex",
          flexDirection:"row",
          borderWidth:1,
          borderColor:'#ced4da',
          borderRadius:5,
          padding:15,
          width:'90%',
          alignSelf:'center',
          marginTop:20
      
      },
      socialText:{
      
          color:'gray',
          fontFamily: 'Montserrat-Light',
          fontSize:13,
          justifyContent:'center',
          alignSelf:'center',
          marginLeft:'10%'
      },
      checkbox:{
      
          
          display:"flex",
          marginLeft:"5%",
          justifyContent:'center',
          marginTop:15
      },
      pickerStyle:{
        
          height: 150,  
          width: "80%", 
          borderWidth:1, 
          borderColor:'#000',
          justifyContent: 'center', 
          alignSelf:'center' 
      },
      
      wide_logo:{
         resizeMode:'contain',
          width:200,
          height:100,
          alignSelf:'center',
      }, 
      modalView: {
        marginTop:320,
        backgroundColor: "#fcfcfc", 
        height:"60%",
        borderRadius: 20,
        padding: 35,
        shadowColor: "#000",
        shadowOffset: { 
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flex:0
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      
      row: {
        flex: 1,
        flexDirection: 'row',
        height:100,
      },
      
      row1: {
        flex: 1,
        flexDirection: 'row',
        height:100,
        marginBottom:100,
      },
      box: {
        flex: 1,
       
        
      }, 
      
      box_image: {
        flex: 1,
        marginLeft:40,
        alignContent: 'center'
      },
      box2: {
        backgroundColor: 'green'
      },
      box3: {
        backgroundColor: 'orange'
      },
      img_country : { 
        height:30,
        width:30,
        borderRadius:50
      },
      two: {
        flex: 1,
        alignContent: 'flex-start',
        marginRight:80
      }
      
    });

export default styles;