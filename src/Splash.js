import React,{Component} from 'react'
import { ImageBackground, View, StyleSheet} from 'react-native';

export default class Splash extends React.Component{
	constructor(){

    super();	
    
     setTimeout(()=>{
            this.setState({current_screen : 'Home'})
        },3000);
    }
    
	render(){
		return(
			
              <ImageBackground source={require('../images/Splash.png')}  style = {styles.backgroundImage} />
		)
	}
}
const styles = StyleSheet.create({
    backgroundImage : {
        resizeMode: 'cover',
        flex:1,
        width:null,
        height:null
    },
  });