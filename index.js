/**
 * @format
 */
import React,{Component} from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import Login from './src/Login';
import Home from './src/Home';
import Splash from './src/Splash';
import Navigation from './src/Navigation';
import AsyncStorage from '@react-native-community/async-storage';
import {name as appName} from './app.json';
console.disableYellowBox = true;
class SplashScreen extends Component{
    constructor(){
        super();
        this.state = {
            current_screen : 'Splash'
        };
       
    }

    componentDidMount= async() => { 
        let value =await AsyncStorage.getItem('@user-data');
        if (value !== null) {
            const userdata = JSON.parse(value);
            this.getHomeUrl(userdata); 
          }

        setTimeout(()=>{
            this.setState({current_screen : 'Home'})
        },3000);
    } 


    
    getHomeUrl = (data) =>{
        AsyncStorage.setItem('@user-data', JSON.stringify(data))
        let obj ={
          method: 'POST',
          headers: {     
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
            // post to laravel need to be json stringify 
          body: JSON.stringify(data)
        }
        fetch('https://moveer.org/api/login/mobile', obj)
        .then((response)=>response.json())
        .then((json)=>{
          console.log(json, " Data = = ==  == =");
          if(json.status_code == 200){
            AsyncStorage.setItem('@login-url',JSON.stringify(json));
            setTimeout(()=>{
                this.setState({current_screen : 'Home'})
            },3000);
          }
        }).catch((error)=> {
           console.log('There has been a problem with your fetch operation: ' + error.message);
        });
    }

    render(){
		let mainScreen = this.state.current_screen == 'Splash' ? <Splash/> : <Navigation/>;
		return mainScreen;
	}
}
AppRegistry.registerComponent(appName, () =>SplashScreen );
